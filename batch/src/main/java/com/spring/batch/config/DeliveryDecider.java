package com.spring.batch.config;

import java.time.LocalDateTime;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;

public class DeliveryDecider implements JobExecutionDecider {

	@Override
	public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
		String resultado = LocalDateTime.now().getHour() < 22 ? "PRESENTE":"NO_PRESENTE";
		System.out.println("El resultado de la decición es : "+resultado);
		return new FlowExecutionStatus(resultado);
	}

}
