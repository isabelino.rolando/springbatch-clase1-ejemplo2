package com.spring.batch.config;

import java.util.Random;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;

public class ReceipDecider implements JobExecutionDecider {

	@Override
	public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
		//creamos un random para emular que podemos o no recibir el producto correcto
		String exitCode = new Random().nextFloat() < .70f ? "CORRECTO" : "INCORRECTO";
		System.out.println("El articulo entregado es : "+exitCode);
		return new FlowExecutionStatus(exitCode);
	}

}
