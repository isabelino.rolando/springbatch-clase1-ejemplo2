package com.spring.batch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableBatchProcessing
@Configuration
public class BatchConfig {
	//constructor de trabajos
	@Autowired
	public JobBuilderFactory jobBuilderFactory;
	//constructor de pasos
	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	
	//agregamos un jobExecutionDecider y llamamos a la clase
	//DeliveryDecider para retornar si recibimos o en un horario especifico de entrega
	@Bean
	public JobExecutionDecider decider() {
		return new DeliveryDecider();
	}
	//agregamos un jobExecutionDecider y llamamos a la clase
	//ReceipDecider para retornar si recibimos o no el articulo correcto
	@Bean
	public JobExecutionDecider receipDecider() {
		return new ReceipDecider();
	}
	
	
	//metodo de preparacion de un paso-agradecer al cliente
	@Bean
	public Step thankCustomerStep() {
					return this.stepBuilderFactory.get("thankCustomerStep").tasklet(new Tasklet() {

						@Override
						public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
							System.out.print("Agradeciendo al cliente");
							return RepeatStatus.FINISHED;
						}
					}).build();
	}
		
	//metodo de preparacion de un paso-reembolso
	@Bean
	public Step refundStep() {
					return this.stepBuilderFactory.get("refundStep").tasklet(new Tasklet() {

						@Override
						public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
							System.out.print("Devolución del dinero al cliente");
							return RepeatStatus.FINISHED;
						}
					}).build();
	}
	
	//metodo de preparacion de un paso-dejar en puerta
	@Bean
	public Step leaveAtDoorStep() {
				return this.stepBuilderFactory.get("leaveAtDoorStep").tasklet(new Tasklet() {

					@Override
					public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
						System.out.print("dejando el paquete en la puerta");
						return RepeatStatus.FINISHED;
					}
				}).build();
	}
	
	//metodo de preparacion de un paso-almacenar producto
	@Bean
	public Step storePackageStep() {
			return this.stepBuilderFactory.get("storePackageStep").tasklet(new Tasklet() {

				@Override
				public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
					System.out.print("almacenar el paquete mientras la dirección del cliente es localizado");
					return RepeatStatus.FINISHED;
				}
			}).build();
	}

		
	
	//metodo de preparacion de un paso-entrega de producto al cliente
	@Bean
	public Step givePackageToCustomerStep() {
		return this.stepBuilderFactory.get("givePackageToCustomer").tasklet(new Tasklet() {

			@Override
			public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				System.out.print("Entrega del paquete al cliente");
				return RepeatStatus.FINISHED;
			}
		}).build();
	}

	
	boolean GOT_LOST = false;
	//metodo de preparacion de un paso-entrega de producto a direccion
	@Bean
	public Step driveToAddressStep() {
		return this.stepBuilderFactory.get("driveToAddressStep").tasklet(new Tasklet() {

			@Override
			public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				//lanzamos una excepcion en tiempo de ejecucion
				if(GOT_LOST) {
					throw new RuntimeException("se perdió conduciendo a la dirección.");
				}
				
				System.out.print("llegó con éxito a la dirección.");
				return RepeatStatus.FINISHED;
			}
		}).build();
	}

	//metodo de preparacion de un paso-empaquetado de producto
	@Bean
	public Step packageItemStep() {
		return this.stepBuilderFactory.get("packageItemStep").tasklet(new Tasklet() {

			@Override
			public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				//agregamos estas cadenas para capturar paramentros de trabajos
				//String item = chunkContext.getStepContext().getJobParameters().get("item").toString();
				//String date = chunkContext.getStepContext().getJobParameters().get("run.date").toString();
				//System.out.print(String.format("El articulo %s ha sido empaquetado a las %s...",item,date));
				System.out.print("El articulo ha sido empaquetado...");
				return RepeatStatus.FINISHED;
			}
			
		}).build();
	}
	
	//metodo para lanzar un job
	@Bean
	public Job deliverPackageJob() {
		return  this.jobBuilderFactory.get("trabajo entrega de paquete10")
				//.incrementer(new RunIdIncrementer())
				.start(packageItemStep())//empaquetado del producto
				.next(driveToAddressStep())//entrega en la direccion
					//.on("FAILED").to(storePackageStep())
				     .on("FAILED").fail()
				.from(driveToAddressStep())
					.on("*").to(decider())
						.on("PRESENTE").to(givePackageToCustomerStep())
							.next(receipDecider()).on("CORRECTO").to(thankCustomerStep())
							.from(receipDecider()).on("INCORRECTO").to(refundStep())
					.from(decider())
						.on("NO_PRESENTE").to(leaveAtDoorStep())
					
				.end()
				
				//.next(givePackageToCustomerStep())//entra el producto al cliente
				.build();
	}

}
